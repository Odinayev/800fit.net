const getters = {
    id: state => state.auth.id,
    token: state => state.auth.token,
    phone: state => state.auth.phone,
    username: state => state.auth.username,
    role: state => state.auth.role,
    userId: state => state.auth.userId,
    notify: state => state.auth.notify,
    locale: state => state.local.locale,
    cart: state => state.auth.cart,
    email: state => state.local.email,
    optionCart: state => state.cart.option,
    basketCart: state => state.cart.basket,
    multiBasketCart: state => state.cart.multibasket,
    address: state => state.cart.address,
    provider: state => state.cart.provider,
    request_type: state => state.cart.request_type,
    comment: state => state.cart.comment,
    inputs: state => state.cart.inputs,
    category_id: state => state.cart.category_id,
    cities: state => state.cities.cities,
    city_id: state => state.cities.city_id
}

export default getters
