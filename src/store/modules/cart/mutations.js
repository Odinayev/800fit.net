import { INPUTS, CATEGORI_ID, COMMENT ,REQUEST_TYPE, PROVIDER, CART, BASKET, BASKET_MULTI, ADDRESS, AddInputs, AddCategoryId, AddComment, AddRequestType,  AddCartAddress, AddCartOptions, AddCartMultiBasket, AddCartBasket, RemoveCache,
AddCart, OrderContent,OrderAddress, AddOrderProviders, ClearCart, AddOrderRequestType} from './../../../utils/cart'

export const mutations = {
// ADD CART MUTATIONS
    ADD_CART: (state, data) => {

        if (state.cart.length != 0) {
            for (let i = 0; i < state.cart.length; i++) {
                const element = state.cart[i];

                if (element.attribute_id === data.attribute_id) {
                    let basket = element.baskets;

                    for (let j = 0; j < basket.length; j++) {

                        const cartBasket = basket[j];
                        let dataBasket = data.baskets;

                        if (cartBasket.basket_id === dataBasket.basket_id) {

                            for (let k = 0; k < cartBasket.items.length; k++) {

                                const cartItem = cartBasket.items[k];
                                let dataItems = dataBasket.items

                                if (cartItem.item_id === dataItems.item_id) {

                                    // state.cart[i].baskets[j].items[k] = dataItems
                                    state.cart[i].baskets[j].items.splice(k,1)
                                    state.cart[i].baskets[j].items.push(dataItems)

                                }
                                else{
                                    let x = cartBasket.items.filter(item => {
                                        return item.item_id === dataItems.item_id
                                    })
                                    if (x[0] && x[0]['item_id']) {
                                    }
                                    else{
                                        cartBasket['items'].push(dataItems)
                                    }
                                }

                            }

                        }
                        else{
                            let s = basket.filter(item => {
                                return item.basket_id === dataBasket.basket_id
                            })
                            if (s[0] && s[0]['basket_id']) {
                            }
                            else{
                                let itemdata = {
                                    basket_id: dataBasket.basket_id,
                                    items:[dataBasket.items]
                                }
                                element.baskets.push(itemdata)
                            }
                        }
                    }
                    if (basket.length == 0) {
                        let itemdata = {
                            basket_id: data.baskets.basket_id,
                            items:[data.baskets.items]
                        }
                        element.baskets.push(itemdata)
                    }


                }
                else{
                    let arr = {
                        attribute_id: data.attribute_id,
                        attribute_name: data.attribute_name,
                        attribute_question: '',
                        baskets:[
                            {
                                basket_id: data.baskets.basket_id,
                                items: [data.baskets.items]

                            }
                        ],
                        options: [],
                        inputs:[]
                    }
                    let attr = state.cart.filter(item => {
                        return item.attribute_id == arr.attribute_id
                    })
                    if (attr[0] && attr[0]['attribute_id']) {
                    }else{
                        state.cart.push(arr);
                    }
                }
            }
        }
        else{
            let arr = {
                attribute_id: data.attribute_id,
                attribute_name: data.attribute_name,
                attribute_question: '',
                    baskets:[
                        {
                        basket_id: data.baskets.basket_id,
                        items: [data.baskets.items]
                    }
                ],
                options: [],
                inputs:[]
            }
            state.cart.push(arr);
        }
        // console.log(state.cart);
        // return
        // state.cart = data
        AddCart(state.cart);
    },

    ADD_CART_OPTION:(state, data) => {
        // state.cart

        if (state.cart.length !== 0) {
            for (let i = 0; i < state.cart.length; i++) {
                const attribute = state.cart[i];
                if (attribute.attribute_id === data.attribute_id) {

                    if (state.cart[i].options) {
                        state.cart[i].attribute_question = data.attribute_question;
                        state.cart[i].attribute_name = state.cart[i].attribute_name ? state.cart[i].attribute_name: data.attribute_name;
                        state.cart[i].options = data.options;

                        if (state.cart[i].options.length == 0 && state.cart[i].baskets.length == 0) {
                            state.cart.splice(i,1);
                        }
                    }

                }
                else{
                    let arr = {
                        attribute_id: data.attribute_id,
                        attribute_name: data.attribute_name,
                        attribute_question: data.attribute_question,
                        baskets:[],
                        options: data.options,
                        inputs:[]
                    }

                    let attr = state.cart.filter(item => {
                        return item.attribute_id == arr.attribute_id
                    })
                    if (attr[0] && attr[0]['attribute_id']) {
                    }else{
                        state.cart.push(arr);
                    }
                }
            }
        }
        else{
            let arr = {
                attribute_id: data.attribute_id,
                attribute_name: data.attribute_name,
                attribute_question: data.attribute_question,
                baskets:[],
                options: data.options,
                inputs:[]
            }
            state.cart.push(arr);
        }

        // console.log(state.cart);
        AddCart(state.cart);

    },

    ADD_CART_INPUT: (state, data )=>{

        if (state.cart.length !== 0) {
            for (let i = 0; i < state.cart.length; i++) {
                const attribute = state.cart[i];
                if (attribute.attribute_id === data.attribute_id) {

                    if (state.cart[i].inputs) {
                        state.cart[i].inputs.splice(0,state.cart[i].inputs.length);
                        state.cart[i].inputs.push({
                            value: data.value,
                            service_input_id: data.service_input_id ,
                            label: data.label,
                        })
                    }

                }
                else{
                    let arr = {
                        attribute_id: data.attribute_id,
                        attribute_name: data.attribute_name,
                        attribute_question: data.attribute_question,
                        baskets:[],
                        options: [],
                        inputs: [{
                            value: data.value,
                            service_input_id: data.service_input_id ,
                            label: data.label,
                        }],
                    }
                    let attr = state.cart.filter(item => {
                        return item.attribute_id == arr.attribute_id
                    })
                    if (attr[0] && attr[0]['attribute_id']) {
                    }else{
                        state.cart.push(arr);
                    }
                }
            }
        }
        else{
            let arr = {
                attribute_id: data.attribute_id,
                attribute_name: data.attribute_name,
                attribute_question: data.attribute_question,
                baskets:[],
                options: [],
                inputs: [{
                    value: data.value,
                    service_input_id: data.service_input_id ,
                    label: data.label,
                }],
            }
            state.cart.push(arr);
        }

        // console.log(state.cart);
        AddCart(state.cart); 
    },

    ADD_CART_PROVIDER: (state, data) => {
        data.price = 0
        data.discount = 0
        state.cart_provider.forEach(function(element,key) {
            if(element.item_id == data.item_id) {
                state.cart_provider.splice(key,1)
            }
        });
        if(data.service_id)
            state.cart_provider.push(data);
    },
    EMPTY_CART_PROVIDER: (state) => {
        state.cart_provider = []
    },

    ADD_CART_OPTION_PROVIDER:(state, data) => {
        // state.cart_provider
    
        if (state.cart_provider.length !== 0) {
            for (let i = 0; i < state.cart_provider.length; i++) {
                const attribute = state.cart_provider[i];
                if (attribute.attribute_id === data.attribute_id) {
                
                    if (state.cart_provider[i].options) {
                        state.cart_provider[i].attribute_question = data.attribute_question;
                        state.cart_provider[i].attribute_name = state.cart_provider[i].attribute_name ? state.cart_provider[i].attribute_name: data.attribute_name;
                        state.cart_provider[i].options = data.options;
    
                        if (state.cart_provider[i].options.length == 0 && state.cart_provider[i].baskets.length == 0) {
                            state.cart_provider.splice(i,1);
                        }
                    }
                    
                }
                else{
                    let arr = {
                        attribute_id: data.attribute_id,
                        attribute_name: data.attribute_name,
                        attribute_question: data.attribute_question,
                        baskets:[],
                        options: data.options,
                        inputs:[]
                    }
                    
                    let attr = state.cart_provider.filter(item => {
                        return item.attribute_id == arr.attribute_id
                    })
                    if (attr[0] && attr[0]['attribute_id']) {
                    }else{
                        state.cart_provider.push(arr);
                    }
                }
            }
        }
        else{
            let arr = {
                attribute_id: data.attribute_id,
                attribute_name: data.attribute_name,
                attribute_question: data.attribute_question,
                baskets:[],
                options: data.options,
                inputs:[]
            }
            state.cart_provider.push(arr);
        }
    
        
    },
    
    ADD_CART_INPUT_PROVIDER: (state, data )=>{
    
        if (state.cart_provider.length !== 0) {
            for (let i = 0; i < state.cart_provider.length; i++) {
                const attribute = state.cart_provider[i];
                if (attribute.attribute_id === data.attribute_id) {
                
                    if (state.cart_provider[i].inputs) {
                        state.cart_provider[i].inputs.splice(0,state.cart_provider[i].inputs.length);
                        state.cart_provider[i].inputs.push({
                            value: data.value,
                            service_input_id: data.service_input_id ,
                            label: data.label,
                        })
                    }
                    
                }
                else{
                    let arr = {
                        attribute_id: data.attribute_id,
                        attribute_name: data.attribute_name,
                        attribute_question: data.attribute_question,
                        baskets:[],
                        options: [],
                        inputs: [{
                            value: data.value,
                            service_input_id: data.service_input_id ,
                            label: data.label,
                        }],
                    }
                    let attr = state.cart_provider.filter(item => {
                        return item.attribute_id == arr.attribute_id
                    })
                    if (attr[0] && attr[0]['attribute_id']) {
                    }else{
                        state.cart_provider.push(arr);
                    }
                }
            }
        }
        else{
            let arr = {
                attribute_id: data.attribute_id,
                attribute_name: data.attribute_name,
                attribute_question: data.attribute_question,
                baskets:[],
                options: [],
                inputs: [{
                    value: data.value,
                    service_input_id: data.service_input_id ,
                    label: data.label,
                }],
            }
            state.cart_provider.push(arr);
        }
    
        // console.log(state.cart_provider);
    },

    ADD_ORDER_COMMENT: (state, data) => {
        state.order_content = data
        OrderContent(state.order_content); 
    },

    ADD_ORDER_ADDRESS: (state, data) => {
        state.order_address = data
        OrderAddress(state.order_address); 
    },

    ADD_ORDER_PROVIDER: (state, data) => {
        state.provider = data
        AddOrderProviders(state.provider); 
    },

    ADD_ORDER_REQUEST_TYPE: (state, data) => {
        state.request_type = data
        AddOrderRequestType(state.request_type); 
    },
// END
// REMOVE CART MUTATIONS

    REMOVE_BASKET_ITEM: (state, item_id) => {
        
        for (let i = 0; i < state.cart.length; i++) {
            const attribute = state.cart[i];
            for (let j = 0; j < attribute.baskets.length; j++) {
                const basket = attribute.baskets[j];
                for (let k = 0; k < basket.items.length; k++) {
                    const item = basket.items[k];

                    if (item.item_id === item_id) {
                        basket.items.splice(k,1);
                    }
                    
                }
                if (basket.items.length === 0) {
                    attribute.baskets.splice(j,1)
                }
            }
            if (attribute.baskets.length === 0 && attribute.options.length === 0) {
                state.cart.splice(i,1)
            }
        }

        AddCart(state.cart);
    },

    REMOVE_OPTION_ITEM: (state, attribute_id) => {
        for (let i = 0; i < state.cart.length; i++) {
            const attribute = state.cart[i];
            
            if (attribute.attribute_id === attribute_id) {
                state.cart[i].options = [];

                if (state.cart[i].options.length == 0 && state.cart[i].baskets.length == 0) {
                    state.cart.splice(i,1);
                }
            }
        }

        AddCart(state.cart);
    },

    REMOVE_INPUT_ITEM: (state, attribute_id) => {

        for (let i = 0; i < state.cart.length; i++) {
            const attribute = state.cart[i];
            
            if (attribute.attribute_id === attribute_id) {
                state.cart[i].inputs = [];

                if (state.cart[i].options.length == 0 && state.cart[i].baskets.length == 0) {
                    state.cart.splice(i,1);
                }
            }
        }

        AddCart(state.cart);
    },

    
    REMOVE_PROVIDER: (state) => {
        let data = '' 
        AddOrderProviders(data); 
        state.provider = data
    },
// END
    CLEAR_BASKET: (state) => {
        state.cart = [];
        state.request_type = '';
        state.order_address = '';
        state.order_content = '';
        state.provider = '';
        ClearCart();
    },  













    SET_ADD_INPUTS: (state, data) => {        
        if (data != '' && data != [] && data != undefined && data != null) {            
            let notElement = true
            for (let index = 0; index < state.inputs.length; index++) {                    
                const element = state.inputs[index];
                if (element.attr_id == data.attr_id) {
                    state.inputs[index] = data;
                    notElement = false
                }                
            }
            if (notElement) {
                state.inputs.push(data)
            }
        }
        AddInputs(state.inputs);
    },

    SET_ADD_CATEGORY_ID: (state, data) => {        
        AddCategoryId(data); 
        state.category_id = data
    },


    SET_REQUEST_TYPE: (state, data) => {
        AddRequestType(data); 
        state.request_type = data
    },

    SET_CART_ADDRESS: (state, data) => {
        AddCartAddress(data); 
        state.address = data
    },

    SET_CART: (state, data) => {
        let cart = state.option;
        for (const key in cart.options) {
            const element = cart.options[key];
            if (cart.options.hasOwnProperty(key)) {
                if (element.attribute_id == data.attribute_id) {
                    cart.options.splice(key,1)
                }                   
            }
        }    
        let opt;
        if(Array.isArray(data.options[data.attribute_id])){
            opt = data.options[data.attribute_id];
        }
        else{
            opt = [data.options[data.attribute_id]]
        }
        
        cart.options.push({
            attribute_id: data.attribute_id,
            attributeName: data.attributeName,
            attributeQuest: data.attributeQuest,
            options: opt
        })            
        state.option = cart;

        for (const key in state.option) {
            const element = state.option[key];
            for (const key in element) {
                if (element.hasOwnProperty(key)) {
                    const element1 = element[key];
                    if(element1.options.length == 0){
                        element.splice(key,1)
                    }
                }
            }
        }

        AddCartOptions(cart); 
        
    },

    SET_CART_BASKET: (state, data) => {
        let basket = state.basket;
        for (const key in basket.data) {
            if (basket.data.hasOwnProperty(key)) {
                const element = basket.data[key];
                if (element.item_ID == data.item_ID) {

                    if(element.SelectedServices.id == data.SelectedServices.id){
                        basket.data.splice(key,1)
                    }
                    
                }                  
            }
        }    
        basket.data.push(data)
        state.basket = basket;
        AddCartBasket(state.basket);    
        
    },

    SET_CART_BASKET_REMOVE: (state, id) => {
        let basket = state.basket;

        for (const key in basket.data) {
            const element = basket.data[key];
            if (basket.data.hasOwnProperty(key)) {
                if (basket.data[key].SelectedServices.id == id) {
                    basket.data.splice(key,1)
                }
            }
        }
        AddCartBasket(basket);
    },

    SET_CART_BASKET_MULTI: (state, data) => {
        let multibasket = state.multibasket;
        for (const key in multibasket.data) {
            if (multibasket.data.hasOwnProperty(key)) {
                const element = multibasket.data[key];
                if (element.item_ID == data.item_ID) {
                    multibasket.data.splice(key,1)
                }                   
            }
        }    

        multibasket.data.push(data)        
        state.multibasket = multibasket;
        AddCartMultiBasket(state.multibasket);    
    },

    SET_CART_MULTI_BASKET_REMOVE: (state, id) => {
        let multibasket = state.multibasket;

        for (const key in multibasket.data) {
            if (multibasket.data.hasOwnProperty(key)) {
                const element = multibasket.data[key];
                if (element.item_ID == id) {
                    multibasket.data.splice(key,1)
                }
            }
        }
        AddCartMultiBasket(multibasket);
    },

    SET_CART_OPTION_REMOVE: (state, id) => {
        let cart = state.option;
        for (const key in cart.options) {
            if (cart.options.hasOwnProperty(key)) {
                const element = cart.options[key];
                if (element.attributeId == id) {
                    cart.options.splice(key,1)
                }                   
            }
        }
        AddCartOptions(cart); 
    },

    REMOVE_CACHE: (state) => {
        RemoveCache()
    },


};
