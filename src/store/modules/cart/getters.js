export const getters = {
    category_id: state=> state.category_id,
    comment: state=> state.comment,
    inputs: state=> state.inputs,
    request_type: state=> state.request_type,
    address: state=> state.address,
    optionCart: state=> state.option,
    basketCart: state=> state.basket,
    multiBasketCart: state=> state.multibasket,
    
    cart: state => state.cart,
    order_content: state => state.order_content,
    order_address: state => state.order_address,
    provider: state=> state.provider,
    request_type: state=> state.request_type,
    cart_provider: state => state.cart_provider,
};
