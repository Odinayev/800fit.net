import { INPUTS, CATEGORI_ID, COMMENT ,REQUEST_TYPE, PROVIDER, CART, BASKET, BASKET_MULTI, ADDRESS, AddInputs, AddCategoryId, AddComment, AddRequestType, AddCartProviders, AddCartAddress, AddCartOptions, AddCartMultiBasket, AddCartBasket, RemoveCache,
GetCart, GetOrderContent, GetOrderAddress, getRequestType} from './../../../utils/cart'
export const state = {
    category_id: CATEGORI_ID(),
    // comment: COMMENT(),
    inputs: INPUTS(),
    request_type: REQUEST_TYPE(),
    address: ADDRESS(),
    option: CART(),
    basket: BASKET(),
    multibasket: BASKET_MULTI(),
    
    cart: GetCart(),
    order: GetOrderContent(),
    order_address: GetOrderAddress(),
    provider: PROVIDER(),
    request_type: getRequestType(),
    cart_provider : []
};
