import { index, store, show, update, closedCart, statuses, changeStatus, changePrice, lastMonthIncome} from './../../../api/carts';

export const actions = {

//  ADD CART ACTIONS
    setCartBasket({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_CART', data)
        resolve()
        })
    },

    setCartOption({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_CART_OPTION', data)
        resolve()
        })
    },

    setCartInput({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_CART_INPUT', data)
        resolve()
        })
    },

    setCartBasketProvider({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_CART_PROVIDER', data)
        resolve()
        })
    },

    cartProviderEmpty({commit}) {
        return new Promise((resolve, reject) => {
            commit('EMPTY_CART_PROVIDER')
            resolve()
        })
    },

    setCartOptionProvider({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_CART_OPTION_PROVIDER', data)
        resolve()
        })
    },

    setCartInputProvider({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_CART_INPUT_PROVIDER', data)
        resolve()
        })
    },

    setOrderContent({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_ORDER_COMMENT', data)
        resolve()
        })
    },

    setOrderAddress({commit}, data) {
        return new Promise((resolve, reject) => {
        commit('ADD_ORDER_ADDRESS', data)
        resolve()
        })
    },

    setProviderData({commit}, data) {
        return new Promise((resolve, reject) => {
            commit('ADD_ORDER_PROVIDER', data)
            resolve()
        })
    },

    RequestType({commit}, data) {
        return new Promise((resolve, reject) => {
            commit('ADD_ORDER_REQUEST_TYPE', data)
            resolve()
        })
    },
// END 
// REMOVE CART ACTIONS
    removeBasketItem({commit},id){
        return new Promise((resolve, reject) => {
            commit('REMOVE_BASKET_ITEM', id)
            resolve()
        })
    },

    removeOptionItem({commit},id){
        return new Promise((resolve, reject) => {
            commit('REMOVE_OPTION_ITEM', id)
            resolve()
        })
    },

    removeInputsItem({commit},id){
        return new Promise((resolve, reject) => {
            commit('REMOVE_INPUT_ITEM', id)
            resolve()
        })
    },

    removeProvider({commit}){
        return new Promise((resolve, reject) => {
            commit('REMOVE_PROVIDER')
            resolve()
        })
    },
// END
// CLEAR BASKET
    clearBasket({commit}){
        return new Promise((resolve, reject) => {
            commit('CLEAR_BASKET')
            resolve()
        })
    },
// END






    lastMonthIncome({commit}, data) {
        return new Promise((resolve, reject) => {
            lastMonthIncome(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err.response.data)
            })
        })
    },


  changeStatus({commit}, data) {
    return new Promise((resolve, reject) => {
        changeStatus(data).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  changePrice({commit}, data) {
    return new Promise((resolve, reject) => {
        changePrice(data).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  statuses({commit}, data) {
      return new Promise((resolve, reject) => {
          statuses(data).then(res => {
              resolve(res.data)
          }).catch(err => {
              reject(err.response.data)
          })
      })
  },

//   addInputs({commit}, data) {
//       return new Promise((resolve, reject) => {
//           commit('SET_ADD_INPUTS', data)
//           resolve()
//       })
//   },
//   addContent({commit}, data) {
//       return new Promise((resolve, reject) => {
//           commit('SET_ADD_COMMENT', data.textarea)
//           commit('SET_ADD_CATEGORY_ID', data.category_id)
//           resolve()
//       })
//   },
//   requestType({commit}, data) {
//       return new Promise((resolve, reject) => {
//           commit('SET_REQUEST_TYPE', data)
//           resolve()
//       })
//   },



//   basketAddress({commit}, data) {
//       return new Promise((resolve, reject) => {
//           commit('SET_CART_ADDRESS', data)
//           resolve()
//       })
//   },
//   removeAddress({commit}) {
//       return new Promise((resolve, reject) => {
//           commit('SET_CART_ADDRESS', [])
//           resolve()
//       })
//   },

//   SelectedOptions({commit}, data) {
//       return new Promise((resolve, reject) => {
//           commit('SET_CART', data)
//           resolve()
//       })
//   },
//   SelectedBasket({commit}, data) {
//       return new Promise((resolve, reject) => {
//           // if(data.multiselection){
//               commit('SET_CART_BASKET_MULTI', data)
//           // }
//           // else{
//           //     commit('SET_CART_BASKET', data)
//           // }
//           resolve()
//       })
//   },



  

//   removeMultiBasketItem({commit},id){
//       return new Promise((resolve, reject) => {
//           commit('SET_CART_MULTI_BASKET_REMOVE', id)
//           resolve()
//       })
//   },

//   removeCache({commit}){
//       return new Promise((resolve, reject) => {
//           commit('REMOVE_CACHE')
//           resolve()
//       })
//   },

  index({ commit }, data) {
      return new Promise((resolve,reject) => {
          index(data).then(res => {
              resolve(res.data)
          }).catch(err => {
              reject(err)
          })
      })
  },
  store({ commit }, data) {
    return new Promise((resolve, reject) => {
        store(data)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                console.log(err.response.data);
                
                reject(err.response.data);
            });
        });
    },
  show({commit},data) {
      return new Promise((resolve,reject) => {
          show(data).then(res => {
              resolve(res.data)
          }).catch(err => {
              reject(err.response)
          })
      })
  },
  closedCart({commit},id) {
      return new Promise((resolve,reject) => {
          closedCart(id).then(res => {
              resolve(res.data.result)
          }).catch(err => {
              reject(err.response.data)
          })
      })
  },
  update({commit},data) {
      return new Promise((resolve,reject) => {
          update(data).then(res => {
              resolve(res.data.result)
          }).catch(err => {
              reject(err.response.data)
          })
      })
  }
  
}

