import { actions } from './slider/actions'
import { getters } from './slider/getters'
import { state } from './slider/state'
import { mutations } from './slider/mutations'

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
