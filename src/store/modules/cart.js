import { actions } from './cart/actions'
import { getters } from './cart/getters'
import { state } from './cart/state'
import { mutations } from './cart/mutations'

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
