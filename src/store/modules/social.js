import { index } from '../../api/social';

const actions = {

  index({ commit }) {
    return new Promise((resolve, reject) => {
      index().then(res => {
            resolve(res.data)
      }).catch(err => {
            reject(err)
      })
    })
  },
}

export default {
  namespaced: true,
  actions
}
