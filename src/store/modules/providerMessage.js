import { store } from '../../api/providerMessage';

const actions = {

    store({commit},data) {
        return new Promise((resolve,reject) => {
            store(data).then(res => {
                resolve(res.data.result)
            }).catch(err => {
                reject(err.response.data)
            })
        })
      },  
  
  }
  
  export default {
    namespaced: true,
    actions
  }