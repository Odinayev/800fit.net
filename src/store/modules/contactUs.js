import { index, show, } from '../../api/contactUs';

const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
            resolve(res.data)
      }).catch(err => {
            reject(err)
      })
    })
  },


}

export default {
  namespaced: true,
  actions
}
