import {model} from "./properties/model";

export const mutations = {
    SET_LIST: (state, categories) => (state.list = categories),
    SET_SLIDERS: (state, list) => (state.list = list),
    SET_MODEL: (state,model) => {
        state.model.id = model.id;
        state.model.name = model.name;
        state.model.parent_id = (model.parent) ? model.parent.id : '';
        state.model.created_at = model.created_at;
        state.model.updated_at = model.updated_at;
    },
    EMPTY_MODEL: (state) => {
        state.model = JSON.parse( JSON.stringify( model ));
    },


};
