import { index, show } from '../../../api/slider';

export const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
        commit("SET_LIST", res.data.result.data.slider.children);
        resolve(res.data.result)
      }).catch(err => {
            reject(err)
      })
    })
  },

  show({commit},id) {
    return new Promise((resolve,reject) => {
        show(id).then(res => {
          commit("SET_SLIDERS", res.data.data.slider.children.data);
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },
  
}