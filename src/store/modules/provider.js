import { list, requests, rating, receivedOrders, findProvider, changePrices, removeService } from '../../api/provider';

const actions = {

    removeService({ commit }, data) {
        return new Promise((resolve, reject) => {
            removeService(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err)
            })
        })
    },

    changePrices({ commit }, data) {
        return new Promise((resolve, reject) => {
            changePrices(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err)
            })
        })
    },

    receivedOrders({ commit }, data) {
        return new Promise((resolve, reject) => {
            receivedOrders(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err)
            })
        })
    },

    getProvider({ commit }, data) {
        return new Promise((resolve, reject) => {
            list(data).then(res => {
                resolve(res.data.result)
            }).catch(err => {
                reject(err)
            })
        })
    },

    findProvider({ commit }, data) {
        return new Promise((resolve, reject) => {
            findProvider(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err)
            })
        })
    },

  requests({commit},data) {
    return new Promise((resolve,reject) => {
        requests(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  rating({commit},data) {
    return new Promise((resolve,reject) => {
        rating(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },  

}

export default {
  namespaced: true,
  actions
}