import {model} from "./properties/model";
import { AddCategory } from "./../../../utils/cart";

export const mutations = {
    SET_LIST: (state, categories) => (state.list = categories),
    SET_PROVIDER_CATEGORIES: (state, categories) => (state.list = categories),
    SET_SUB_CATEGORIES: (state, categories) => (state.list = categories),
    SET_MODEL: (state,model) => {
        state.model.id = model.id;
        state.model.name = model.name;
        state.model.parent_id = (model.parent) ? model.parent.id : '';
        state.model.created_at = model.created_at;
        state.model.updated_at = model.updated_at;
    },
    EMPTY_MODEL: (state) => {
        state.model = JSON.parse( JSON.stringify( model ));
    },

    SET_CATEGORY: (state, data) => {
        state.cart_category = data
        AddCategory(state.cart_category); 
    },
    SET_BACKGRAMS: (state, data) => {
        
        if (data.backgrams === false) {
            state.backgrams.splice(0,state.backgrams.length)
        }

        let arr = state.backgrams
        let count = state.backgrams.length
        if (count !== 0) {
            
            for (let i = 0; i < count; i++) {
                const element = arr[i];
                
                if (element.id === data.id) {
                    arr.splice(i,arr.length);
                }
                if (arr.length === 0) {
                    arr.push({
                        id: data.id,
                        category_title: data.category_title,
                        image: data.image,
                    })
                }
                else{
                    let x = arr.filter(item => {
                        return item.id === data.id
                    })
                    if (x[0] && x[0]['id']) {
                    }
                    else{
                        arr.push({
                            id: data.id,
                            category_title: data.category_title,
                            image: data.image
                        })
                    }

                }
            }
        }
        else{
            arr.push({
                id: data.id,
                category_title: data.category_title,
                image: data.image
            })
        }

        state.backgrams = arr;
    }


};
