export const getters = {
    list: state => state.list,
    model: state => state.model,
    providerCategories: state => state.providerCategories,
    subcategories: state => state.subcategories,
    cart_category: state => state.cart_category,
    backgrams: state => state.backgrams
};
