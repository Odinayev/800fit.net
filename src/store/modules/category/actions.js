import { index, show, subcategories, providerCategory, children, popular, search, getAttributes } from '../../../api/category';

export const actions = {

    index({ commit }, data) {
      return new Promise((resolve, reject) => {
        index(data).then(res => {
          commit('SET_LIST',res.data.data.categories)
              resolve(res.data)
        }).catch(err => {
              reject(err)
        })
      })
    },
  
    providerCategories({ commit }, data) {
      return new Promise((resolve, reject) => {
        providerCategory(data).then(res => {
              resolve(res.data)
        }).catch(err => {
              reject(err)
        })
      })
    },
  
    show({commit},id) {
      return new Promise((resolve,reject) => {
          show(id).then(res => {
              resolve(res.data)
          }).catch(err => {
              reject(err.response)
          })
      })
    },

    children({commit},data) {
      return new Promise((resolve,reject) => {
          children(data).then(res => {
            resolve(res.data)
            commit('SET_BACKGRAMS', data);
          }).catch(err => {
              reject(err.response)
          })
      })
    },

    popular({commit},data) {
      return new Promise((resolve,reject) => {
          popular(data).then(res => {
              resolve(res.data)
          }).catch(err => {
              reject(err.response)
          })
      })
    },
  
    subcategories({commit},data) {
      return new Promise((resolve, reject) => {
        subcategories(data).then(res => {
              resolve(res.data)
        }).catch(err => {
              reject(err)
        })
      })
    },

    search({commit},data) {
      return new Promise((resolve, reject) => {
        search(data).then(res => {
              resolve(res.data)
        }).catch(err => {
              reject(err)
        })
      })
    },

    setCategoryData({commit},data) {
      return new Promise((resolve, reject) => {
        commit('SET_CATEGORY', data)
      })
    },
  
    getAttributes({commit},data) {
      return new Promise((resolve,reject) => {
          getAttributes(data).then(res => {
            resolve(res.data)
          }).catch(err => {
              reject(err.response)
          })
      })
    },
}