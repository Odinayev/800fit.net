import {model} from "./properties/model";
import { getCategory } from "./../../../utils/cart";

export const state = {
    list: [],
    model: model,
    providerCategories: [],
    subcategories: [],
    backgrams: [],
    cart_category: getCategory(),
};
