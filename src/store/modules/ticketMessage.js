import { store } from '../../api/ticketMessage';

const actions = {

    store({commit},data) {
        return new Promise((resolve,reject) => {
            store(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err.response.data)
            })
        })
      },  
  
  }
  
  export default {
    namespaced: true,
    actions
  }