import {  } from './../../utils/cart'
import { index, show, } from '../../api/notify';

const state = {
  notifications: []
}

const getters = {
  notifications: state=> state.notifications,
}

const mutations = {
  NOTIFY_DATA: (state, notifications) => {
      state.notifications = notifications
  }
}

const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
        commit('NOTIFY_DATA', res.data.data.notifications)
        resolve(res.data)
      }).catch(err => {
        console.log(err);
        
        reject(err)
      })
    })
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}