import { store, showByCategory } from '../../api/providerService';

const actions = {

    store({commit},data) {
        return new Promise((resolve,reject) => {
            store(data).then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err.response.data)
            })
        })
    },  
    showByCategory({commit},data) {
        return new Promise((resolve,reject) => {
            showByCategory(data).then(res => {
                resolve(res.data.data.result)
            }).catch(err => {
                reject(err)
            })
        })
    },
  }
  
  export default {
    namespaced: true,
    actions
  }