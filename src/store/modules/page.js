import { index, show, } from '../../api/page';

const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
            resolve(res.data)
      }).catch(err => {
            reject(err)
      })
    })
  },

  show({commit},id) {
    return new Promise((resolve,reject) => {
        show(id).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

}

export default {
  namespaced: true,
  actions
}
