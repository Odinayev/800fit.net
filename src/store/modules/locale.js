
import { getLocale, setLocale } from './../../utils/auth'

const state = {
  locale: getLocale(),
}

const mutations = {
  SET_TOKEN: (state, locale) => {
    state.locale = locale
  },
}

const actions = {  

  getLocale({},locale) {
    setLocale(locale)
  },


}


export default {
  namespaced: true,
  state,
  mutations,
  actions
}
