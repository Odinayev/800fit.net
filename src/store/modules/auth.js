// import { index, list, show } from './../../api/notification'
import { login, logout, refresh, getInfo, updateUserProfile,updatePassword, register, resetPassword,confirmPassword, loginSocialite } from './../../api/auth'
import { getToken, getUserName, getPhone, getUserDateBirth, getUserAddress, setUserData, setRole, setToken,  removeToken, getRole, removeUserData, getUserId, getFirstName, setNotification, getNotification, getId, getLastName, getEmail, getUserImage } from './../../utils/auth'


const state = {
  id: getId(),
  token: getToken(),
  username: getUserName(),
  firstname: getFirstName(),
  phone: getPhone(),
  role: getRole(),
  userId: getUserId(),
  notify: getNotification(),
  lastname: getLastName(),
  email: getEmail(),
  date_birth: getUserDateBirth(),
  address: getUserAddress(),
  image: getUserImage(),
}

const getters = {
  id: state=> state.id,
  username: state => state.username,
  firstname: state => state.firstname,
  phone: state => state.phone,
  role: state => state.role,
  token: state => state.token,
  email: state => state.email,
  lastname: state => state.lastname,
  date_birth: state => state.date_birth,
  address: state => state.address,
  image: state => state.image
}

const mutations = {
  SET_ID: (state, id) => {
    state.id = id
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USER_DATA: (state, user) => {
    state.id = user.id
    state.username = user.username
    state.firstname = user.first_name
    state.lastname = user.last_name
    state.email = user.email
    state.address = user.address
    state.date_birth = user.date_birth
    state.phone = user.phone
    state.image = user.image
  },
  REMOVE_USER_DATA: (state,user) => {
    state.username = user
    state.firstname = user
    state.lastname = user
    state.email = user
    state.address = user
    state.date_birth = user
    state.phone = user
    state.image = user
  },
  SET_FIRSTNAME: (state, firstname) => {
    state.firstname = firstname
  },
  SET_USERNAME: (state, username) => {
    state.username = username
  },
  SET_ROLE: (state, role) => {
    state.role = role
  },
  SET_PHONE: (state, phone) => {
    state.phone = phone
  },
  SET_NOTIFY: (state,number) => {
    state.notify = number
  }
}

let refreshToken = () => {
  refresh().then(res => {
    const { data } = res.data
    if (data.token) {
      setToken(data.token)
      setUserData(data.user)
      (data.user.role=='') ? commit('SET_ROLE', 'user') : commit('SET_ROLE', data.user.role.data.name) 
      getToken()
      getRole()
      let expiresIn = data.expires_in * 1000; // time in milliseconds
      setTimeout(refreshToken, expiresIn - 5000); // refreshes token 5 seconds earlier before expiration
    }
  }).catch(err=>{
    removeToken()
  })
};


const actions = {  

  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        var data = response.data.data;
        if (data.token) {
          commit('SET_TOKEN', data.token)
          commit('SET_PHONE', data.user.phone)
          commit('SET_USERNAME', data.user.username)
          commit('SET_ROLE', data.role)

          setToken(data.token)
          setRole(data.role)
          setUserData(data.user)
          commit('SET_USER_DATA', data.user)
          getUserId(data.user.id)
          resolve()
        }
      }).catch(err => {    
        console.log(err)
        reject(err.response.data)
      })
    })
  },
  register({commit},data) {
    return new Promise((resolve,reject) => {
        register(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },  
  updateUserProfile({commit}, data) {
    return new Promise((resolve,reject) => {
      updateUserProfile(data).then(res => {
        commit('SET_USER_DATA', res.data.data)
        setUserData(res.data.data)
            resolve(res.data)
        }).catch(err => {
            reject(err.response)
        })
    })
  },

  getInfo({commit}) {
    return new Promise((resolve,reject) => {
        getInfo().then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response)
        })
    })
  },

  updateProfile({commit}, data){
    return new Promise((resolve, reject) => {
      setUserData(data.data.result.data.user)
      commit('SET_USER_DATA', data.data.result.data.user)
      resolve()
    })
    .catch(err => {    
      reject(err.response.data)
    })
  },

  updatePassword({commit}, data){
    return new Promise((resolve,reject) => {
      updatePassword(data).then(res => {
          resolve(res.data)
        }).catch(err => {
          reject(err.response)
        })
    })
  },

  resetPassword({commit}, data){
    return new Promise((resolve,reject) => {
      resetPassword(data).then(res => {
          resolve(res.data)
        }).catch(err => {
          reject(err.response)
        })
    })
  },

  confirmPassword({commit}, data){
    return new Promise((resolve,reject) => {
      confirmPassword(data).then(res => {
          resolve(res.data)
        }).catch(err => {
          reject(err.response)
        })
    })
  },

  loginSocialite({commit}, data){
    return new Promise((resolve,reject) => {
      loginSocialite(data).then(res => {
        console.log(res);
        
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  },

  registerLogin({commit}, data) {
      var dataUser = data.data
      const { username, password } = dataUser.user
      return new Promise((resolve, reject) => {
        login({ username: username.trim(), password: password }).then(response => {
          var data = response.data.result.data;
          if (data.token) {
  
            commit('SET_TOKEN', data.token)
            commit('SET_PHONE', data.user.phone)
            commit('SET_USERNAME', data.user.username)
  
            if(data.user.role.data){
              commit('SET_ROLE', data.user.role.data.name)
            }
            else{
              commit('SET_ROLE', "user")
            }
  
            setToken(data.token)
            setUserData(data.user)
            getUserId(data.user.id)
            let expiresIn = data.expires_in * 1000; // time in milliseconds
            setTimeout(refreshToken, expiresIn - 5000); // refreshes token 5 seconds earlier before expiration
            resolve()
          }
        }).catch(err => {    
          reject(err.response.data)
        })
      })
  },

  // user logout
  logout({ commit, state }) {
    
    return new Promise((resolve, reject) => {
      logout().then(() => {
        commit('SET_ID', '')
        commit('SET_TOKEN', '')
        commit('SET_USERNAME', '')
        commit('SET_FIRSTNAME', '')
        commit('SET_ROLE', '')
        commit('SET_PHONE', '')
        removeToken()
        removeUserData()
        commit('REMOVE_USER_DATA','')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_PHONE', '')
      commit('SET_ROLE', '')
      removeToken()
      removeUserData()
      resolve()
    })
  },

 
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
