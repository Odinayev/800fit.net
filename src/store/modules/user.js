import { index, restore, code, show, search, store, senCart, update, address, addressUpdate, destroy, updateProfile, updatePassword, carts } from '../../api/user';

const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
            resolve(res.data.result)
      }).catch(err => {
            reject(err.response.data)
      })
    })
  },

  carts({ commit }, data) {
    return new Promise((resolve, reject) => {
      carts(data).then(res => {
            resolve(res.data)
      }).catch(err => {
            reject(err.response.data)
      })
    })
  },

  search({commit},data){
    return new Promise((resolve,reject) => {
        search(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
          reject(err.response.data)
        })
    })
  },

  show({commit},id) {
    return new Promise((resolve,reject) => {
        show(id).then(res => {
            resolve(res.data.result)
        }).catch(err => {
          reject(err.response.data)
        })
    })
  },

  updateProfile({commit},data) {
    return new Promise((resolve,reject) => {
      updateProfile(data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  }, 

  updatePassword({commit},data) {
    return new Promise((resolve,reject) => {
      updatePassword(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  }, 

  store({commit},data) {
    return new Promise((resolve,reject) => {
        store(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  senCart({commit},data) {
    return new Promise((resolve,reject) => {
      senCart(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  update({commit},data) {
    return new Promise((resolve,reject) => {
        update(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
          reject(err.response.data)
        })
    })
  },

  destroy({commit},id) {
    return new Promise((resolve,reject) => {
        destroy(id).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },
  
  address({commit},id) {
    return new Promise((resolve,reject) => {
      address(id).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  addressUpdate({commit},data) {
    return new Promise((resolve,reject) => {
      addressUpdate(data).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  restore({ commit }, data) {
    return new Promise((resolve, reject) => {
      restore(data).then(res => {
            resolve(res.data.result)
      }).catch(err => {
            reject(err.response.data)
      })
    })
  },

  code({ commit }, data) {
    return new Promise((resolve, reject) => {
      code(data).then(res => {
            resolve(res.data.result)
      }).catch(err => {
            reject(err.response.data)
      })
    })
  },
}

export default {
  namespaced: true,
  actions
}
