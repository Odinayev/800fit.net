import { index } from '../../api/translate';

const actions = {

  index({ commit }) {
    return new Promise((resolve, reject) => {
      index().then(res => {
            resolve(res.data.result)
      }).catch(err => {
            reject(err.response.data)
      })
    })
  },

}

export default {
  namespaced: true,
  actions
}