import { index, show, store, close } from '../../api/tickets';

const state = {
  list: [],
  pagination: {
    count: 20,
    current_page: 1,
    per_page: 20,
    total: 101,
    total_pages: 6
  }
}

const getters = {
  list: state=> state.list,
  pagination: state=> state.pagination,
  
}

const mutations = {
  SET_LIST: (state,data) => {    
    state.list = data.tickets;
    state.pagination = data.pagination;
  },
  SET_PAGINATION:(state, data) => {
    state.pagination.current_page = data
  }
}


const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
        commit('SET_LIST', res.data.data)
            resolve(res.data)
      }).catch(err => {
            reject(err)
      })
    })
  },

  show({commit},id) {
    return new Promise((resolve,reject) => {
        show(id).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  close({commit},id) {
    return new Promise((resolve,reject) => {
        close(id).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  store({commit},data) {
    return new Promise((resolve,reject) => {
        store(data).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  setPagination({commit},data) {
    commit('SET_PAGINATION', data)
  },

}

export default {
  namespaced: true,
  actions,
  state,
  mutations,
  getters
}
