import { index, show, } from '../../api/cities';
import { getCities, setCities, getCityId, setCityId } from './../../utils/cities'

const state = {
  cities: getCities(),
  city_id: getCityId(),
}
const getters = {
  cities: state=> state.cities,
  city_id: state=> state.city_id,
}
const mutations = {
  SET_CITIES: (state, data) => {
    state.cities = data.data.cities
    setCities(data.data.cities)
  },

  SET_CITY_ID: (state, data) => {
    state.city_id = {city_id:data.id, city_name: data.name}
    setCityId(data)
  },
}
const actions = {

  index({ commit }, data) {
    return new Promise((resolve, reject) => {
      index(data).then(res => {
        commit('SET_CITIES', res.data)
        resolve(res.data)
      }).catch(err => {
            reject(err)
      })
    })
  },

  show({commit},id) {
    return new Promise((resolve,reject) => {
        show(id).then(res => {
            resolve(res.data.result)
        }).catch(err => {
            reject(err.response.data)
        })
    })
  },

  setCitiId({commit},data) {
    return new Promise((resolve,reject) => {
      commit('SET_CITY_ID', data)
      setCityId(data)
      resolve()
    })
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
