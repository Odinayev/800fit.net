import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./plugins/element.js";
import "../public/css/main.css";
import i18n from "./plugins/i18n/i18n";
window.jQuery = window.$ = require("jquery");
var waypoints = require("waypoints/lib/jquery.waypoints.js");
import "./components/BaseComponents/index";
import * as VueGoogleMaps from "vue2-google-maps";

// import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueSocialauth from 'vue-social-auth'
import axios from 'axios';

Vue.use(VueAxios, axios)
Vue.use(VueSocialauth, {
 
  // providers: {
  //   github: {
  //     clientId: '',
  //     redirectUri: '/auth/github/callback' // Your client app URL
  //   }
  // }
  providers: {
    facebook: {
      client_id: '2500301940034855',
      client_secret: 'XX1b9c429d964656198ecc6ead2e904564X',
      redirectUri: '/auth/login/facebook/callback'
    },
    github: {
      clientId: '',
      redirectUri: '/auth/github/callback' // Your client app URL
    },
    google: {
      clientId: '507929545377-soshnp94cotsg2neer65u0rmscq8dhil.apps.googleusercontent.com',
      redirectUri: '/auth/google/callback' // Your client app URL
    },
    twitter: {
      client_id: '2500301940034855',
      client_secret: 'XX1b9c429d964656198ecc6ead2e904564X',
      redirect: '/auth/login/twitter/callback'
    }
  },
    
});

window.jQuery = window.$ = jQuery;
Vue.config.productionTip = false;
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDoS6XkizqN2Pun-M9yU7p4tSGY46Mbgfs",
    libraries: "places" // necessary for places input
  }
});

Vue.store = Vue.prototype.dir = i18n.locale === "ar" ? "rtl" : "ltr";

if (i18n.locale === "ar") {
  $("body").addClass("active");
} else {
  $("body").removeClass("active");
}

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
