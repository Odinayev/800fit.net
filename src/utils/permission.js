import store from './../store/index'

/**
 * @param {Array} permissions
 * @returns {Boolean}
 * @example see @/views/permission/directive.vue
 */
export default function checkPermission(permissions) {
    const per_role = localStorage.getItem('auth_user_role');
    if (per_role) {
        let role  = JSON.parse(per_role);
        if (typeof role == 'object') {
            if (permissions && permissions instanceof Array && permissions.length > 0) {
                let hasPermission = true;
                for (let i = 0; i < permissions.length; i++) {
                    const permission = permissions[i];
                    if (role.permissions.data) {
                        hasPermission = hasPermission * role.permissions.data.some(perm => perm.slug === permission)
                    }
                }
                return hasPermission;
            } else {
                if (role.permissions.data) {
                    return role.permissions.data.some(perm => perm.slug === permissions)
                }
            }
          return false;
        }
    }
    return false;
}
