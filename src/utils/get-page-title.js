import defaultSettings from './../settings'

const title = defaultSettings.title || 'Admin 800fix'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
