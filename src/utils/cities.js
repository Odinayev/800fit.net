const cities = 'cities'
const city_id = 'city_id'

export function setCities(data) {
    return localStorage.setItem(cities, JSON.stringify(data))
}
export function getCities() {
    let city = localStorage.getItem(cities)
    if (city !== '' && city !== undefined && city !== null) {
        return JSON.parse(city); 
    } else {
        return null;
    }
}

export function setCityId(data) {
    let city = {city_id:data.id, city_name: data.name}
    return localStorage.setItem(city_id, JSON.stringify(city))
}

export function getCityId() {
    let data = localStorage.getItem(city_id)
    return JSON.parse(data)
}