const IdKey = 'auth_user_id'
const TokenKey = 'auth_user_access_token'
const UserNameKey = 'auth_user_username'
const UserFirstName = 'auth_user_firstname'
const UserLastName = 'auth_user_lastname'
const PhoneKey = 'auth_user_phone'
const RoleKey = 'auth_user_role'
const UserKeyId = 'auth_user_id'
const Notification = 'notification'
const Locale = 'locale'
const UserEmail = 'auth_user_email'
const UserDateBirth = 'auth_user_date_birth'
const UserAddress = 'auth_user_address'
const UserImage = 'auth_user_image'

export function getId() {
  return localStorage.getItem(IdKey)
}

export function getToken() {
  return localStorage.getItem(TokenKey)
}

export function setLocale(locale) {
  return localStorage.setItem(Locale, locale)
}

export function setNotification(notify) {
  return localStorage.setItem(Notification, notify)
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, token)
}

export function setRole(role) {
  localStorage.setItem(RoleKey,role)
  return true
}

export function setUserData(user) {
  localStorage.setItem(UserNameKey,user.username)
  localStorage.setItem(PhoneKey,user.phone)
  localStorage.setItem(UserKeyId,user.id)
  localStorage.setItem(UserFirstName,user.first_name)
  localStorage.setItem(UserLastName,user.last_name)
  localStorage.setItem(UserEmail,user.email)
  localStorage.setItem(UserDateBirth,user.date_birth)
  localStorage.setItem(UserAddress,user.address)
  localStorage.setItem(UserImage,user.image)

  return true
}

export function getUserName() {
  return localStorage.getItem(UserNameKey)
}

export function getFirstName() {
  return localStorage.getItem(UserFirstName)
}

export function getLastName() {
  return localStorage.getItem(UserLastName)
}

export function getEmail(){
  return localStorage.getItem(UserEmail)
}

export function getUserDateBirth(){
  return localStorage.getItem(UserDateBirth)
}

export function getUserAddress(){
  return localStorage.getItem(UserAddress)
}

export function getUserImage(){
  return localStorage.getItem(UserImage)
}

export function getLocale() {
  return localStorage.getItem(Locale)
}

export function getUserId() {
  return localStorage.getItem(UserKeyId)
}

export function getNotification() {
  return localStorage.getItem(Notification)
}

export function getPhone() {
  return localStorage.getItem(PhoneKey)
}

export function getRole() {
  return localStorage.getItem(RoleKey)
}

export function removeToken() {
  localStorage.removeItem(TokenKey)
  localStorage.removeItem(UserNameKey)
  localStorage.removeItem(PhoneKey)
  localStorage.removeItem(RoleKey)
  return true
}

export function removeUserData() {
  localStorage.removeItem(UserNameKey)
  localStorage.removeItem(PhoneKey)
  localStorage.removeItem(RoleKey)
  localStorage.removeItem(UserKeyId)
  localStorage.removeItem(UserFirstName)
  localStorage.removeItem(UserLastName)
  localStorage.removeItem(UserEmail)
  localStorage.removeItem(UserDateBirth)
  localStorage.removeItem(UserAddress)
  localStorage.removeItem(UserImage)
  return true
}