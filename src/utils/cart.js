
const category_id = 'category_id'
const comment = 'comment'
const inputs = 'inputs'
const address = 'address'
const optioncart = 'optioncart'
const basketcart = 'basketcart'
const multibasketcart = 'multibasketcart'

const cart = 'cart'
const cart_provider = 'cart_provider'
const order_content = 'order_content'
const order_address = 'order_address'
const provider = 'provider'
const cart_category = 'cart_category'
const request_type = 'request_type'


export function AddCart(data) {
    localStorage.setItem(cart,JSON.stringify(data));
}

export function AddCartProvider(data) {
    localStorage.setItem(cart_provider,JSON.stringify(data));
}

export function GetCart() {
    let cart1 = localStorage.getItem(cart);
    if (cart1) {
        return JSON.parse(cart1);
    }else{
        return []
    }
}

export function OrderContent(data){
    localStorage.setItem(order_content,data);
}

export function GetOrderContent() {
   return  localStorage.getItem(order_content);
}

export function OrderAddress(data){
    localStorage.setItem(order_address,JSON.stringify(data));
}

export function GetOrderAddress() {
   let data = localStorage.getItem(order_address);
   return JSON.parse(data);
}

export function AddOrderProviders(data){
    localStorage.setItem(provider,JSON.stringify(data));
}

export function PROVIDER() {
    let getprovider = localStorage.getItem(provider);      
    if (getprovider) {
        return JSON.parse(getprovider);
    }else{
        return null;
    }
}

export function AddCategory(data){
    localStorage.setItem(cart_category,JSON.stringify(data));
}

export function getCategory() {
    let category = localStorage.getItem(cart_category);      
    if (category) {
        return JSON.parse(category);
    }else{
        return null;
    }
}

export function AddOrderRequestType(data){
    localStorage.setItem(request_type, data);
}

export function getRequestType() {
    let request = localStorage.getItem(request_type);      
    return request ? request : '';
}

export function ClearCart() {
    localStorage.removeItem(cart);
    localStorage.removeItem(order_content);
    localStorage.removeItem(order_address);
    localStorage.removeItem(provider);
    localStorage.removeItem(cart_category);
    localStorage.removeItem(request_type);
}





//  ************** END ****************** //
export function AddCartOptions(data) {    
    localStorage.setItem(optioncart,JSON.stringify(data));
}

export function CART() {
    let cart = localStorage.getItem(optioncart);          
    if (cart !== '' && cart !== undefined && cart !== null) {
        return JSON.parse(cart);
    }else{
        return {
            options: []
        }
    }
}


export function AddCartBasket(data){
    localStorage.setItem(basketcart,JSON.stringify(data));
}

export function AddCartMultiBasket(data){
    localStorage.setItem(multibasketcart,JSON.stringify(data));
}

export function BASKET_MULTI() {
    let cart = localStorage.getItem(multibasketcart);  
    if (cart !== '' && cart !== undefined && cart !== null) {
        return JSON.parse(cart);
    }else{
        return {
            data: []
        }
    }
}

export function BASKET() {
    let cart = localStorage.getItem(basketcart);  
    if (cart !== '' && cart !== undefined && cart !== null) {
        return JSON.parse(cart);
    }else{
        return {
            data: []
        }
    }
}

export function AddCartAddress(data){
    localStorage.setItem(address,JSON.stringify(data));
}

export function ADDRESS() {
    let getAddress = localStorage.getItem(address);      
    if (getAddress !== '' && getAddress !== undefined && getAddress !== null) {
        return JSON.parse(getAddress);
    }else{
        return {
            getAddress: []
        }
    }
}



export function AddRequestType(data){
    localStorage.setItem(request_type,data);
}

export function REQUEST_TYPE() {
   return  localStorage.getItem(request_type);
}



export function AddInputs(data){
    localStorage.setItem(inputs,JSON.stringify(data));
}

export function INPUTS() {
    let input = localStorage.getItem(inputs);        
    if (input !== '' && input !== undefined && input !== null) {
        return JSON.parse(input);
    }else{
        return  []
    }
}

export function AddCategoryId(data){
    localStorage.setItem(category_id,data);
}

export function CATEGORI_ID() {
   return  localStorage.getItem(category_id);
}

export function RemoveCache() {
    localStorage.removeItem(category_id);
    localStorage.removeItem(comment);
    localStorage.removeItem(provider);
    localStorage.removeItem(address);
    localStorage.removeItem(optioncart);
    localStorage.removeItem(multibasketcart);
    localStorage.removeItem(request_type);
    localStorage.removeItem(basketcart);
    localStorage.removeItem(inputs);
    return true
 }