import request from './../utils/request'

export function login(data) {
  return request({
    url: '/auth/login',
    method: 'post',
    data
  })
}

export function register(data) {
  return request({
    url: '/auth/register',
    method: 'post',
    data
  })
}

export function resetPassword(data) {
  return request({
    url: '/auth/reset-password',
    method: 'post',
    data
  })
}

export function confirmPassword(data) {
  return request({
    url: '/auth/confirm-reset-password',
    method: 'post',
    data
  })
}

export function loginSocialite(params) {
  console.log(params);
  return request({
    url: '/auth/login/' + params,
    method: 'post'
  })
}

export function getInfo() {
  return request({
    url: '/auth/user',
    method: 'get',
  })
}

export function updateUserProfile(data) {
  return request({
    url: '/auth/update-profile',
    method: 'post',
    data
  })
}

export function updatePassword(data) {
  return request({
    url: '/auth/update-password',
    method: 'post',
    data
  })
}

export function refresh() {
  return request({
    url: '/auth/refresh',
    method: 'get'
  })
}


export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post'
  })
}
