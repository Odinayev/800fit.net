import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/notifications',
      method: 'get',
      data
    })
  }
