import request from './../utils/request'

  export function list(params) {
    return request({
      url: '/provider/get-list',
      method: 'get',
      params
    })
  }

  export function requests(data) {    
    return request({
      url: `cart?` + data,
      method: 'get'
      // data
    })
  }

  export function removeService(data) {    
    return request({
      url: `remove-provider-service`,
      method: 'post',
      data
    })
  }

  export function findProvider(data) {
    return request({
      url: `providers/find-provider-post`,
      method: 'post',
      data
    })
  }

  export function rating(data) {
    return request({
      url: '/provider-reting',
      method: 'post',
      data
    })
  }

  export function receivedOrders(params) {
    return request({
      url: '/carts/received-orders',
      method: 'get',
      params
    })
  }

  export function changePrices(data) {
    return request({
      url: '/provider/change-price',
      method: 'post',
      data
    })
  }
