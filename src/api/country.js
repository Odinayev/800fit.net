import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/countries?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) {    
    return request({
      url: `/countries/${params.id}`,
      method: 'get',
      params
    })
  }
