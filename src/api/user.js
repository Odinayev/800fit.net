import request from './../utils/request'

export function index(data) {
    return request({
      url: '/users?' + data,
      method: 'get'
    })
  }

  export function carts(data) {
    return request({
      url: '/carts?' + data,
      method: 'get'
    })
  }

  export function restore(data) {
    return request({
      url: '/user/restore',
      method: 'POST',
      data
    })
  }

  export function code(data) {
    return request({
      url: '/user/code',
      method: 'POST',
      data
    })
  }

export function search(data) {
    return request({
      url: '/users',
      method: 'post',
      params
    })
  }

  export function show(id) {
    return request({
      url: `/users/${id}`,
      method: 'get'
    })
  }

  export function address(id) {
    return request({
      url: `users/address/${id}`,
      method: 'get'
    })
  }

  export function addressUpdate(data) {
    return request({
      url: `users/address/update`,
      method: 'post',
      data
    })
  }

  export function store(data) {
    return request({
      url: '/users',
      method: 'post',
      data
    })
  }

  export function senCart(data) {
    return request({
      url: '/setCart',
      method: 'post',
      data
    })
  }

  export function updateProfile(data) {
    return request({
      url: '/front/auth/update-profile',
      method: 'post',
      data
    })
  }

  updatePassword
  export function updatePassword(data) {
    return request({
      url: '/users/updatePassword',
      method: 'post',
      data
    })
  }

  export function update(data) {
    return request({
      url: `/users/${data.id}`,
      method: 'put',
      data
    })
  }

  export function destroy(id) {
    return request({
      url: `/users/${id}`,
      method: 'delete',
    })
  }

