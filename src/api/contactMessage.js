import request from './../utils/request'

export function store(data) {
    return request({
      url: '/contact-message',
      method: 'post',
      data
    })
  }
