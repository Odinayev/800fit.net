import request from './../utils/request'

export function store(data) {
    return request({
      url: '/calendars',
      method: 'post',
      data
    })
  }

  export function show(id) {    
    return request({
      url: `/calendars/${id}`,
      method: 'get',
    })
  }
