import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/sliders',
      method: 'get',
      data
    })
  }

  export function show(params) {    
    return request({
      url: `/sliders/show`,
      method: 'get',
      params
    })
  }