import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/carts?' + data,
      method: 'get',
    //   data
    })
  }

  export function store(data) {
    return request({
      url: '/carts',
      method: 'post',
      data
    })
  }

  export function statuses(params) {
    return request({
      url: '/carts/statuses?lang=' + params.lang,
      method: 'get'
    })
  }

  export function show(params) {    
    return request({
      url: `/carts/${params.id}`,
      method: 'get',
      params
    })
  }

  export function closedCart(id) {    
    return request({
      url: `/cart/closed-cart/${id}`,
      method: 'get'
    })
  }

  export function update(data) {
    return request({
      url: `/carts/${data.id}`,
      method: 'put',
      data
    })
  }

  export function changeStatus(params) {
    return request({
      url: `/carts/change-status/${params.id}`,
      method: 'put',
      params
    })
  }

  export function changePrice(params) {
    return request({
      url: `/carts/change-price/${params.id}`,
      method: 'put',
      params
    })
  }

  export function lastMonthIncome(params) {
    return request({
      url: `/provider/lastMonth/income`,
      method: 'get',
      params
    })
  }