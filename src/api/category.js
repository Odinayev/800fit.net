import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/categories?' + data,
      method: 'get',
    //   data
    })
  }

  export function providerCategory(data) {
    return request({
      url: '/categories/provider-categories?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) {    
    return request({
      url: `/categories/${params.id}`,
      method: 'get',
      params
    })
  }
  export function subcategories(data) {    
    return request({
      url: `/categories/subcategories?` + data,
      method: 'get',
    })
  }

  export function children(params) {    
    return request({
      url: `/categories/children/` + params.id,
      method: 'get',
      params
    })
  }

  export function getAttributes(params) {    
    return request({
      url: `/categories/get-attributes/` + params.id,
      method: 'get'
    })
  }

  export function popular(data) {    
    return request({
      url: `/categories/popular`,
      method: 'get',
      data
    })
  }

  export function search(params) {
    return request({
      url: `/categories/search`,
      method: 'get',
      params
    })
  }
