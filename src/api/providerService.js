import request from './../utils/request'

export function store(data) {
    return request({
      url: '/provider-services/storeServices',
      method: 'post',
      data
    })
  }

  export function showByCategory(data) {    
    return request({
      url: `/provider-services/showByCategory?` + data,
      method: 'get',
    })
  }