import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/search?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) {    
    return request({
      url: `/search/${params.id}`,
      method: 'get',
      params
    })
  }
