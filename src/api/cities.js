import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/cities?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) {    
    return request({
      url: `/cities/${params.slug}`,
      method: 'get',
      params
    })
  }
