import request from './../utils/request'

  export function index() {
    return request({
      url: '/socials',
      method: 'get'
    })
  }

  export function search(data) {
    return request({
      url: '/social',
      method: 'post',
      params
    })
  }

  export function show(id) {
    return request({
      url: `/social/${id}`,
      method: 'get'
    })
  }

  export function store(data) {
    return request({
      url: '/social',
      method: 'post',
      data
    })
  }

  export function update(data) {
    return request({
      url: `/social/${data.id}`,
      method: 'put',
      data
    })
  }

  export function destroy(id) {
    return request({
      url: `/social/${id}`,
      method: 'delete',
    })
  }
