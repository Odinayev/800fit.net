import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/post?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) {    
    return request({
      url: `/post/${params.id}`,
      method: 'get',
      params
    })
  }
