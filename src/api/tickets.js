import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/tickets?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) { 
    return request({
      url: `/tickets/${params.id}`,
      method: 'get',
      params
    })
  }

  export function close(params) {   
    return request({
      url: `/tickets/close/${params.id}`,
      method: 'put'
    })
  }

  export function store(data) {
    return request({
      url: '/tickets',
      method: 'post',
      data
    })
  }
