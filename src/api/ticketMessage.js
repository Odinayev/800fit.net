import request from './../utils/request'

export function store(data) {
    return request({
      url: '/tickets/ticket-message',
      method: 'post',
      data
    })
  }
