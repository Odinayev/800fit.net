import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/pages?' + data,
      method: 'get',
    //   data
    })
  }

  export function show(params) {    
    return request({
      url: `/pages/${params.slug}`,
      method: 'get',
      params
    })
  }
