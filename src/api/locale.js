import request from './../utils/request'

  export function index(data) {
    return request({
      url: '/locale/' + data,
      method: 'get'
    })
  }

  export function getLocale() {
    return request({
      url: '/locale',
      method: 'post',
      data: 'ar'
    })
  }

