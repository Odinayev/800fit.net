import request from './../utils/request'

export function store(data) {
  return request({
    url: '/provider-messages',
    method: 'post',
    data
  })
}
