import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Service from "./modules/Service";
import Auth from "./modules/Auth";
import ProverDashboard from "./modules/ProviderDashboard";
import User from "./modules/User";
import Provider from "./modules/Provider";
import News from "./modules/News";
import Pages from "./modules/Pages";
import PrivacyPolicy from "./modules/PrivacyPolicy";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/terms-and-condition",
    name: "TermsAndCondition",
    component: () => import("../views/TermsAndConditions")
  },
  {
    path: "/contact",
    name: "Contact",
    component: () => import("../views/Contact")
  },
  {
    path: "/how-it-works",
    name: "HowItWorks",
    component: () => import("../views/HowItWorks")
  },
  {
    path: "/about-us",
    name: "AboutUs",
    component: () => import("../views/AboutUs")
  },
  {
    path: "/404",
    component: () => import("../views/Error")
  },
  {
    path: "*",
    component: () => import("../views/Error")
  },
  ...Service,
  ...Auth,
  ...ProverDashboard,
  ...User,
  ...Provider,
  ...News,
  ...PrivacyPolicy,
  ...Pages,
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior: () => ({
    y: 0
  })
});

export default router;
