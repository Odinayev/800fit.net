const PrivacyPolicy = [
    {
      path: "/privacy-policy",
      name: "privacyPolicy",
      component: () => import("@/views/PrivacyPolicy/index")
    }
  ];
  
  export default PrivacyPolicy;
  