const User = [
  {
    path: "/user",
    name: "UserProfile",
    redirect: "user-profile",
    component: () => import("@/views/User"),
    children: [
      {
        path: "/user-profile",
        name: "Profile",
        component: () => import("@/views/User/Components/Profile")
      },
      {
        path: "/user-request",
        name: "UserRequest",
        component: () => import("@/views/User/Components/Request")
      },
      {
        path: "/show-request/:id",
        name: "UserRequestShow",
        component: () => import("@/views/User/Components/Request/show")
      },
      {
        path: "/user-tickets",
        name: "UserTickets",
        component: () => import("@/views/User/Components/Tickets")
      },
      {
        path: "/show-tickets/:id",
        name: "UserTicketShow",
        component: () => import("@/views/User/Components/Tickets/show")
      }
    ]
  }
];

export default User;
