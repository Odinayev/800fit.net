const Provider = [
  {
    path: "/provider",
    name: "Provider",
    component: () => import("@/views/Provider")
  }
];

export default Provider;
