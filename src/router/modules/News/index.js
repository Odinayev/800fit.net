const News = [
  {
    path: "/news",
    name: "News",
    component: () => import("@/views/News")
  },
  {
    path: "/new/:id",
    name: "NewsShow",
    component: () => import("@/views/News/show")
  }
];

export default News;
