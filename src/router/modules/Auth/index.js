const Auth = [
  {
    path: "/login",
    name: "Login",
    component: () => import("../../../views/Auth/Login")
  },
  {
    path: "/sign-up",
    name: "SignUp",
    component: () => import("../../../views/Auth/Register")
  },
  {
    path: "/reset-password",
    name: "ResetPassword",
    component: () => import("../../../views/Auth/ResetPassword")
  },
  {
    path: '/auth/:provider/callback',
    component: {
      template: '<div class="auth-component"></div>'
    }
  },
];

export default Auth;
