const Service = [
  {
    path: "/service/:id",
    name: "Service",
    component: () => import("@/views/Services/index")
  },
  {
    path: "/time-and-location",
    name: "TimeAndLocation",
    component: () => import("@/views/Services/TimeAndLocation")
  },
  {
    path: "/select-provider",
    name: "SelectProvider",
    component: () => import("@/views/Services/SelectProvider")
  },
  {
    path: "/checkout",
    name: "Checkout",
    component: () => import("@/views/Services/Checkout")
  },
  {
    path: "/accepted",
    name: "Accepted",
    component: () => import("@/views/Services/Accepted")
  },
  {
    path: "/transaction",
    name: "Transaction",
    component: () => import("@/views/Services/Transaction")
  },
  {
    path: "/all-service",
    name: "AllService",
    component: () => import("@/views/Services/AllService")
  },
  {
    path: "/catalog-service/:id",
    name: "catalogService",
    component: () => import("@/views/Services/CatalogService")
  }
];

export default Service;
