const Page = [
    {
      path: "/:slug",
      name: "Page",
      component: () => import("@/views/pages/index")
    }
  ];
  
  export default Page;
  