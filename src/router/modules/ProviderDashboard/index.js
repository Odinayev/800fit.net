const ProverDashboard = [
  {
    path: "/dashboard",
    name: "DashboardHome",
    redirect: "dashboard-home",
    component: () => import("@/views/ProviderDashboard"),
    children: [
      {
        path: "/dashboard-home",
        name: "DashboardIndex",
        component: () => import("@/views/ProviderDashboard/Components/Home"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/dashboard-request",
        name: "DashboardRequest",
        component: () => import("@/views/ProviderDashboard/Components/Requests"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/request-show/:id",
        name: "RequestShow",
        component: () =>
          import("@/views/ProviderDashboard/Components/Requests/show"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/dashboard-tickets",
        name: "DashboardTicket",
        component: () => import("@/views/ProviderDashboard/Components/Tickets"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/ticket-show/:id",
        name: "TicketShow",
        component: () =>
          import("@/views/ProviderDashboard/Components/Tickets/show"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/dashboard-service",
        name: "DashboardService",
        component: () => import("@/views/ProviderDashboard/Components/Service"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/dashboard-service-new",
        name: "DashboardServiceNew",
        component: () => import("@/views/ProviderDashboard/Components/Service/newIndex"),
        meta: {
          hideSearchBar: true
        }
      },
      {
        path: "/dashboard-profile",
        name: "DashboardProfile",
        component: () => import("@/views/ProviderDashboard/Components/Profile"),
        meta: {
          hideSearchBar: true
        }
      }
    ]
  }
];

export default ProverDashboard;
