const webpack = require('webpack'); //to access built-in plugins
const path = require('path')


module.exports = {
    publicPath: '/',
    outputDir: 'dist',
    assetsDir: '',
    // resolve: {
    //     alias: {
    //         'waypoints': 'waypoints/lib'
    //     }
    // },
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                // Popper: ['popper.js', 'default'],
            }),
        ]
    }
}
